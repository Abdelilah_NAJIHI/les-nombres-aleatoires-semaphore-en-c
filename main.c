/*******************Fait par : NAJIHI Abdelilah ****************/
/************Ce mini projet son objectif est de tester la fonction random de la bibliothèque C.********/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <math.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <semaphore.h>

#define NB_ESSAIS_PROCESSUS 1000000 					//La valeur du nombre d'essais par processus
#define NB_PROCESSUS 1000           					//La valeur du nombre de processus
#define RAND_MX 32767              					//La valeur du nombre maximum de la methode random


// ici on va déclarer notre sémaphore

sem_t semaphore;

//Les ProtoTypes
void viderBuffer();
long lire();
void afficherTab(int *tab);
void init(long *tailleTabRandom, long *nbDEssaisParProcessus, long *nbDeProcessus);
void statistics(int *tab, double taille);


//Fonction qui creer une zone mémoire partagee entre les differents processus
void* createSharedMemory(size_t size) {
	int protection = PROT_READ | PROT_WRITE;
	int visibility = MAP_SHARED | MAP_ANONYMOUS;
	return mmap(NULL, size, protection, visibility, -1, 0);
}

int main() {
	
	//Mes Variables
	
	pid_t pid; //Declaration d'un processus
	long tailleTabRandom, nbDEssaisParProcessus, nbDeProcessus;


	
	//Initialisation
	
	//Demande des limites
	init(&tailleTabRandom, &nbDEssaisParProcessus, &nbDeProcessus);
	//Memoire partagee
	int * tabPartager = createSharedMemory(tailleTabRandom*sizeof(int));
	//Initialisation du Semaphore
	sem_init(&semaphore, 0, 1);
	
	for (int i = 0; i < tailleTabRandom ; i++) {
		tabPartager[i] = 0;
	}
	//La Creation des processus avec la boucle do while
	int i = 0;
	do{
		pid = fork();
		usleep(5);
	} while(pid && i++ < nbDeProcessus );


       //Affichage de notre Tableau 
	/*afficherTab(tabPartager);*/
	
	//1 cas Si on est dans le processus pere, on attend la fin de tout les processus fils avant de faire un rapport des statestique.
	if (pid) {
		for (int i = 0; i < nbDeProcessus; i++) {
			wait(NULL);
		};
		sem_destroy(&semaphore);
		statistics(tabPartager, (double)tailleTabRandom);
	} else {
		//2 cas Si on est dans un processus fils, le processus va calculer sa partie du tableau de son coté.
		srand(time(NULL));
		int memoire[nbDEssaisParProcessus];
		for (int i = 0; i < nbDEssaisParProcessus ; i++) {
			*(memoire + i) = rand()%tailleTabRandom;
		}
		//Puis il demande l'acces au sémaphore pour modifier le tableau de la zone partagée afin d'eviter d'ecrire en même
		//temps que les autres processus fils.
		sem_wait(&semaphore);
		// Section critique
		for (int i = 0; i < nbDEssaisParProcessus; i++) {
			tabPartager[memoire[i]]++;
		}
		//A la fin de son travail, il libere le semaphore pour laisser la place a un autre processus.
		sem_post(&semaphore);
		exit(EXIT_SUCCESS);
	}

	return 0;
}

//Affichage du tableau partage
void afficherTab(int *tab){
    int i;
    for(i=0; i<RAND_MX; ++i)   
    printf(" Tab[%d] \t -> \t %d \n",i,tab[i]);
    printf("\n");
}





//Fonction pour vider le buffer de stdin
void viderBuffer() {
	int c = 0;
	while (c != '\n' && c != EOF)
	{
		c = getchar();
	}
}

//Fonction pour lire le texte entrer et le convertir en nombres
long lire() {
	int tailleChar = 8;
	char texte[tailleChar];
	char *posEntree = NULL;
	long resultat;

	if (fgets(texte, tailleChar, stdin) != NULL)
	{
		posEntree = strchr(texte, '\n');
		if (posEntree != NULL)
		{
			*posEntree = '\0';
		}
		else
		{
			viderBuffer();
		}
		resultat = strtol(texte, NULL, 10);
		return resultat;
	}
	else
	{
		printf("Eror ! => nous changeons  par la valeur defaut \n");
		viderBuffer();
		return 0;
	}
}



//Fonction pour demander les valeurs à utiliser.
//Si il y a une erreur, les valeurs par défauts sont utilisé à la place.
void init(long *tailleTabRandom, long *nbDEssaisParProcessus, long *nbDeProcessus) {
	printf("\n++++++++++Entrez la valeur maximum pour la fonction random (par defaut : 32767):");
	*tailleTabRandom = lire();
	if (*tailleTabRandom == 0){
		*tailleTabRandom = RAND_MX;
	}
	printf("\n+++++++++++Entrez le nombre de processus (par defaut : 1000):");
	*nbDeProcessus = lire();
	if (*nbDeProcessus == 0) {
		*nbDeProcessus = NB_PROCESSUS;
	}
	printf("\n++++++++++++Entrez le nombre d'essais par processus (par defaut : 1000000):");
	*nbDEssaisParProcessus = lire();
	if (*nbDEssaisParProcessus == 0) {
		*nbDEssaisParProcessus = NB_ESSAIS_PROCESSUS;
	}
	//Information
	printf("\n--------- Les Données Choisies ----------\n");
	
	
	printf("--------Nombre de processus = %ld \n", *nbDeProcessus);
	printf("--------Taille de l'echantion par processus = %ld \n", *nbDEssaisParProcessus);
	printf("--------Limite du random = %ld \n", *tailleTabRandom);
	printf("\n------------------------------Resultat---------------------------------- \n");
	
	}


//Fonction qui calcule les statistiques du tableau comme la moyenne, la variance, l'ecart type ou encore le coeffient
//de variation.
void statistics(int *tab, double taille) {
	int max = *tab, min = *tab;
	int elementMax = 0, elementMin = 0;
	double somme1 = 0.0f, somme2 = 0.0f;
//On va Determiner notre Valeaur Min et Max et aussi la somme de notre Tab
	for (int i = 0; i < taille ; i++) {
		if (tab[i] > max) {
			max = tab[i];
			elementMax = i;
		}
		if (tab[i] < min) {
			min = tab[i];
			elementMin = i;
		}
		somme1 += tab[i];
		somme2 += tab[i]*tab[i];
	}

	double moyenne = somme1/taille;
	double variance = somme2/taille-moyenne*moyenne;
	double ecartType = sqrt(variance);
	double coefVar = ecartType/moyenne;

	//Affichage
	printf("------La moyenne = %.2f\n", moyenne);
	printf("------La variance = %.2f\n", variance);
	printf("------L Ecart-type = %.2f\n", ecartType);
	printf("------Le coefficient de variation = %.2f\n", coefVar);
	printf("------Le Max = %d \tSaValeur = %d\n", max, elementMax);
	printf("------Le Min = %d \tSaValeur = %d\n", min, elementMin);
	
	//Verification de notre Coefficient de variation pour savoir si notre fonction Random est bien ou pas  
	if(coefVar <= 1.0f) {
		printf("\nSituation de votre Random : La fonction Random du C  est trés bien équilibrée. \n");
	} else {
		if(coefVar <= 5.0f) {
			printf("\nSituation de votre Random : La fonction Random du C est bien équilibrée. \n");
		} else if(coefVar <= 10.0f) {
			printf("\nSituation de votre Random : La fonction Random du C  est  plus ou moins équilibrée. \n");
		} else if(coefVar <= 50.0f) {
			printf("\nSituation de votre Random : La fonction Random du C n'est pas équilibrée.\n");
		}
		printf("Situation de votre Random : Try Again\n");
	}

}
